const _ = require('lodash');

console.log('1)', _.map(['understanding', 'monads'], s => [...s]));
console.log('2)', _.flatten(_.map(['understanding', 'monads'], s => [...s])));
console.log('3)', _.flatMap(['understanding', 'monads'], s => [...s]));

compose = (f, g) => x => f(g(x))

const double = x => 2 * x
const plusone = x => x + 1
console.log('1)', compose(double, plusone)(5));

console.log('2)', double(plusone(5)));

// associativity
// compose(compose(f, g), h) = compose(f, compose(g, h));
const toString = x => `Result: ${x}`;

console.log('3)', compose(compose(toString, double), plusone)(5));
console.log('4)', compose(toString, compose(double, plusone))(5));

// functor is a container which have map() function
// array is a functor

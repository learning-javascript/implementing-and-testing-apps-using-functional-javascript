const isLoggedIn = async () => true;
const getTopTenGoogleBooks = async x => x;
const renderBooks = x => {
  console.log(`${x} has been rendered.`);
};

const populateBooks = async user => {
  const isLogged = await isLoggedIn(user);
  if (isLogged) {
    const [books1, books2] = await Promise.all([
      getTopTenGoogleBooks('JavaScript'),
      getTopTenGoogleBooks('Java')
    ]);
    renderBooks(books1);
    renderBooks(books2);
  }
};

populateBooks('Edu').then(() => console.log('done'));

const asyncSequence = async function () {
  const asyncPromise1 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('1') }, 1000);
  });
  const callbackArgs1 = await asyncPromise1;
  console.log('1) callbackArgs1', callbackArgs1);
  const asyncPromise2 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('2') }, 1000);
  });
  const callbackArgs2 = await asyncPromise2;
  console.log('2) callbackArgs2', callbackArgs2);
  const asyncPromise3 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('3') }, 1000);
  });
  const callbackArgs3 = await asyncPromise3;
  console.log('3) callbackArgs3', callbackArgs3);
};

asyncSequence().then(() => console.log('4) done'));

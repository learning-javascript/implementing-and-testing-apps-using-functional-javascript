const giveMeIceCream = new Promise((resolve, reject) => {
    resolve('Chocolate');
});

giveMeIceCream.then(iceCream => {
    console.log('1) Thank you for giving me', iceCream);
    return 'I am full now';
}).then(statusMessage => {
    console.log('2) Status message:', statusMessage);
    throw   new Error('I wanted strawberry, not chocolate');
}).catch(err => {
    console.log('3) Error:', err.message);
});

const giveMeIceCream1 = new Promise((resolve, reject) => {
    reject(new Error('No shop was found'));
});

giveMeIceCream1.then(iceCream => {
    console.log('4) Thank you for giving me', iceCream);
}).catch(err => {
    console.log('5) Error:', err.message);
});

Promise.resolve('Chocolate').then(iceCream => {
    console.log('6) Flavour:', iceCream);
});

const promisedGame = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Super JavaScript World');
        }, 2000);
});
const promisedIce = Promise.resolve('Chocolate');

Promise.all([promisedGame, promisedIce])
    .then(values => console.log('7) values:', values))
    .catch(error => console.log('8) Error:', error.message));

const rejectedPromisedGame = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('Super JavaScript World');
    }, 2000);
    reject(new Error('Game is out of stock'));
});
Promise.all([rejectedPromisedGame, promisedIce])
    .then(values => console.log('9) values:', values))
    .catch(error => console.log('10) Error:', error.message));

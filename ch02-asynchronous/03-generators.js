const str = 'Iterators';
chArray = [...str];
console.log('1)', chArray);

const stringIterator = str[Symbol.iterator]();
console.log('2)', stringIterator.next());
console.log('3)', stringIterator.next());
console.log('4)', stringIterator.next());
console.log('5)', stringIterator.next());
console.log('6)', stringIterator.next());
console.log('7)', stringIterator.next());
console.log('8)', stringIterator.next());
console.log('9)', stringIterator.next());
console.log('10)', stringIterator.next());
console.log('11)', stringIterator.next());

const daysGenerator = function *() {
  yield 'Monday';
  yield 'Tuesday';
  yield 'Wednesday';
  yield 'Thursday';
  yield 'Friday';
  yield 'Saturday';
  yield 'Sunday';
  return 'We are done';
};

const daysIterator = daysGenerator();
console.log('12)', daysIterator.next());
console.log('13)', daysIterator.next());
console.log('14)', daysIterator.next());
console.log('15)', daysIterator.next());
console.log('16)', daysIterator.next());
console.log('17)', daysIterator.next());
console.log('18)', daysIterator.next());
console.log('19)', daysIterator.next());

const daysIterable = daysGenerator();
console.log('20)', [...daysIterable]);

const generator = function *() {
  const value = yield 1;
  yield value;
};
const it = generator();
console.log('21)', it.next());
console.log('22)', it.next('VALUE'));

const asyncSequence = function *() {
  const asyncPromise1 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('1') }, 1000);
  });
  const callbackArgs1 = yield asyncPromise1;
  console.log('23) callbackArgs1', callbackArgs1);
  const asyncPromise2 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('2') }, 1000);
  });
  const callbackArgs2 = yield asyncPromise2;
  console.log('24) callbackArgs2', callbackArgs2);
  const asyncPromise3 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('3') }, 1000);
  });
  const callbackArgs3 = yield asyncPromise3;
  console.log('25) callbackArgs3', callbackArgs3);
};

const asyncIterator = asyncSequence();

asyncIterator.next().value
  .then(callbackArgs1 =>
    asyncIterator.next(callbackArgs1).value
  ).then(callbackArgs2 =>
    asyncIterator.next(callbackArgs2).value
  ).then(callbackArgs3 =>
    asyncIterator.next(callbackArgs3).value
  );

const asyncIterator1 = asyncSequence();
const executeSequence = (asyncIterator1, callbackArg = null) => {
  const promise = asyncIterator1.next(callbackArg).value;
  if (typeof promise === 'object' && typeof promise.then === 'function') {
    promise.then(newCallbackArg => executeSequence(asyncIterator1, newCallbackArg));
  }
};
executeSequence(asyncIterator1);

const asyncSequence1 = function *() {
  const asyncPromise1 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('4') }, 1000);
  });
  const callbackArgs1 = yield asyncPromise1;
  console.log('26) callbackArgs1', callbackArgs1);
  const asyncPromise2 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('5') }, 1000);
  });
  const callbackArgs2 = yield asyncPromise2;
  console.log('27) callbackArgs2', callbackArgs2);
  const asyncPromise3 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve('6') }, 1000);
  });
  const callbackArgs3 = yield asyncPromise3;
  console.log('28) callbackArgs3', callbackArgs3);
};

const asyncSequence2 = function *() {
  yield *asyncSequence();
  yield *asyncSequence1();
};

const asyncIterator2 = asyncSequence2();
const executeSequence1 = (asyncIterator2, callbackArg = null) => {
  const promise = asyncIterator2.next(callbackArg).value;
  if (typeof promise === 'object' && typeof promise.then === 'function') {
    promise.then(newCallbackArg => executeSequence(asyncIterator2, newCallbackArg));
  }
};
executeSequence1(asyncIterator2);

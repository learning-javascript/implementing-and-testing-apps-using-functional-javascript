const lazyFilter = function *(iterable, filterFunction) {
  for (let elem of iterable) {
    if (filterFunction(elem)) yield elem;
  }
};

const lazySequence = lazyFilter([1, 2, 3, 4, 5], x => x % 2 === 0);
console.log('1)', lazySequence.next().value);
console.log('2)', lazySequence.next().value);
console.log('3)', lazySequence.next());

const lazyMap = function *(iterable, mapFunction) {
  for (let elem of iterable) {
    yield mapFunction(elem);
  }
};

const names = ['takeshi', 'akari', 'hiroe', 'miyabi'];
const capitalize = name => name[0].toUpperCase() + name.slice(1);
const isFiveOrLessCharsLong = name => name.length <= 5;

const mapIterable = lazyMap(names, capitalize);
const filterIterator = lazyFilter(mapIterable, isFiveOrLessCharsLong);

console.log('4)', filterIterator.next());
console.log('5)', filterIterator.next());
console.log('6)', filterIterator.next());

const filterIterable2 = lazyFilter(names, isFiveOrLessCharsLong);
const mapIterator2 = lazyMap(filterIterable2, capitalize);
console.log('7)', mapIterator2.next());
console.log('8)', mapIterator2.next());
console.log('9)', mapIterator2.next());

// infinite sequences
function *idGenerator() {
  let id = 0;
  while (true) yield id++;
}
const getId = idGenerator();
console.log('10)', getId.next());
console.log('11)', getId.next());
console.log('12)', getId.next());

function* take(iterator, numberOfItems) {
  for (let i = 0; i < numberOfItems; i++) {
    const {value, done} = iterator.next();
    if (done) return value;
    yield value;
  }
}
console.log('13)', [...take(getId, 5)]);

function take2(iterator, numberOfItems) {
  let result = [];
  for (let i = 0; i < numberOfItems; i++) {
    const {value, done} = iterator.next();
    if (done) return result;
    result.push(value);
  }
  return result;
}
console.log('14)', [...take2(getId, 10)]);

const lazyMap2 = (arr, mapFunction) => {
  return {
    get: function (index) {
      return mapFunction(arr[index]);
    },
    take: function (n) {
      return arr.slice(0, n).map(mapFunction);
    },
    value: function () {
      return arr.map(mapFunction);
    }
  };
};
const lazyList = lazyMap2([1, 2, 3, 4, 5], x => x * 2);
console.log('15)', lazyList.take(3));
console.log('16)', lazyList.take(2));

const _ = require('lodash');
const lazyList2 = _([1, 2, 3, 4, 5]);
const lazyList3 = lazyList2.map(x => x * 2);
console.log('17)', lazyList3.take(2));
console.log('18)', lazyList3.take(2).value());
console.log('19)', lazyList3.take(2).value());


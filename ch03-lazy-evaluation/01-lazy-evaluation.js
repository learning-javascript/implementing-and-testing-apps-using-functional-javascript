console.log('1)', [1, 2, 3, 4, 5].map(x => x * 2)[0]);

const lazyMap = (arr, mapFunction) => {
  return {
    get: function (index) {
      return mapFunction(arr[index]);
    },
    take: function (n) {
      return arr.slice(0, n).map(mapFunction);
    },
    value: function () {
      return arr.map(mapFunction);
    }
  };
};

console.log('2)', lazyMap([1, 2, 3, 4, 5], x => x * 2).get(0));

console.log('3)', lazyMap([1, 2, 3, 4, 5], x => x * 2).value());

const lazyList = lazyMap([1, 2, 3, 4, 5], x => x * 2);
// in the below example every element is evaluated every time .take() is called
console.log('4)', lazyList.take(3));
console.log('5)', lazyList.take(2));

// this can be optimized with memoization
const memo = f => {
  let memoMap = new Map();

  return fArg => memoMap.has(fArg) ? memoMap.get(fArg) : memoMap.set(fArg, f(fArg)).get(fArg);
};

// lazy load combined with memoization
const lazyMemoMap = (arr, mapFunction) => {
  const memo = [];
  return {
    get: function (index) {
      if (memo[index]) {
        return memo[index];
      }
      const result = mapFunction(arr[index]);
      memo[index] = result;
      return result;
    },
    getMemo: function () {
      return memo;
    }
  };
};

const lazyArray = lazyMemoMap([1, 2, 3, 4, 5], x => x * 2);

console.log('6)', lazyArray.get(1));
console.log('7)', lazyArray.get(4));
console.log('8)', lazyArray.getMemo());

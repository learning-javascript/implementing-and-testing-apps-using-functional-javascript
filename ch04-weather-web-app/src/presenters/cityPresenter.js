import {getTodaysTemplate, getForecastTable} from '../utils/parser';

export const renderCity = ({ today, forecast, timezoneResponse}) => {
  const domElement = document.querySelector('.js-city-weather');
  const todayData = getTodaysTemplate(today, timezoneResponse);
  const forecastData = forecast ? getForecastTable(forecast, timezoneResponse)(0, 4) : '';

  domElement.innerHTML = `${todayData}${forecastData}`;
};

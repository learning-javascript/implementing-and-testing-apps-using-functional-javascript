import {expect} from 'chai';
import {
  getCity,
  getCountry,
  getWeatherData,
  getTodaysTemperatures,
  getSunriseSunset,
  getTodaysTemplate,
  getForecastRow,
  getForecastTable
} from './parser';

describe('Parser util', () => {
  it('should return the city', () => {
    expect(getCity({name: 'City'})).to.equal('City');
  });

  it('should return the country', () => {
    expect(getCountry({
      sys: {
        country: 'COUNTRY_CODE'
      }
    })).to.equal('COUNTRY_CODE');
  });

  it('should return the weather condition and description', () => {
    const input = {
      weather: [
        {
          main: 'Clouds',
          description: 'scattered clouds'
        }
      ]
    };
    const response = 'Clouds (scattered clouds)';
    expect(getWeatherData(input)).to.equal(response);
  });

  it('should return the todays temperatures', () => {
    const input = {
      main: {
        temp: 20,
        temp_min: 18,
        temp_max: 22
      }
    };
    const response = 'Temperature (min, average, max): 18, 20, 22';
    expect(getTodaysTemperatures(input)).to.equal(response);
  });

  it('should return the sunrise and sunset times', () => {
    const weatherInput = {
      sys: {
        sunrise: 1515568256,
        sunset: 1515599797
      }
    };
    const timezoneInput = {
      timezoneId: 'Europe/Zurich'
    };

    const response = 'Sunrise: 08:10, Sunset: 16:56';
    expect(getSunriseSunset(weatherInput, timezoneInput)).to.equal(response);
  });

  it('should return the full template for today', () => {
    const weatherInput = {
      main: {
        temp: 20,
        temp_min: 18,
        temp_max: 22
      },
      weather: [
        {
          main: 'Clouds',
          description: 'scattered clouds'
        }
      ],
      sys: {
        country: 'COUNTRY_CODE',
        sunrise: 1515568256,
        sunset: 1515599797
      },
      name: 'City'
    };
    const timezoneInput = {
      timezoneId: 'Europe/Zurich'
    };
    const response = `
<div>City, COUNTRY_CODE: Clouds (scattered clouds)</div>
<div>Temperature (min, average, max): 18, 20, 22</div>
<div>Sunrise: 08:10, Sunset: 16:56</div>`.trim();
    expect(getTodaysTemplate(weatherInput, timezoneInput)).to.equal(response);
  });

  it('should return the forecast row', () => {
    const forecastResponse = {
      list: [
        {
          dt: 1497661200,
          temp: { day: 20 },
          weather: [{
            main: 'Sunny',
            description: 'sky is clear'
          }]
        },
        {
          dt: 1497747600,
          temp: { day: 16 },
          weather: [{
            main: 'Clouds',
            description: 'broken clouds'
          }]
        }
      ]
    };
    const timezoneResponse = {
      timezoneId: 'Europe/Zurich'
    };

    const firstRow = getForecastRow(forecastResponse, timezoneResponse)(0);
    const secondRow = getForecastRow(forecastResponse, timezoneResponse)(1);

    const expectedFirstRow = `
<tr>
    <td>Jun 17th</td>
    <td>20</td>
    <td>Sunny (sky is clear)</td>
</tr>`.trim();
    const expectedSecondRow = `
<tr>
    <td>Jun 18th</td>
    <td>16</td>
    <td>Clouds (broken clouds)</td>
</tr>`.trim();

    expect(firstRow).to.equal(expectedFirstRow);
    expect(secondRow).to.equal(expectedSecondRow);
  });

  xit('should return empty string if index specifies the forecast row which does not exist', () => {});

  it('should return the forecast table', () => {
    const forecastResponse = {
      list: [
        {
          dt: 1497661200,
          temp: { day: 20 },
          weather: [{
            main: 'Sunny',
            description: 'sky is clear'
          }]
        },
        {
          dt: 1497747600,
          temp: { day: 16 },
          weather: [{
            main: 'Clouds',
            description: 'broken clouds'
          }]
        }
      ]
    };
    const timezoneResponse = {
      timezoneId: 'Europe/Zurich'
    };

    const table = getForecastTable(forecastResponse, timezoneResponse)(0, 1);

    const expectedResult = `
<table>
<tr>
    <th>Date</th>
    <th>Temperature (C)</th>
    <th>Weather description</th>
</tr>

<tr>
    <td>Jun 17th</td>
    <td>20</td>
    <td>Sunny (sky is clear)</td>
</tr>
<tr>
    <td>Jun 18th</td>
    <td>16</td>
    <td>Clouds (broken clouds)</td>
</tr>
</table>`.trim();

    expect(table).to.equal(expectedResult);
  });
});

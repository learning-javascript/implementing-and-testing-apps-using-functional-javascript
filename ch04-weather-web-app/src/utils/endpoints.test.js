import {expect} from 'chai';
import {WEATHER_API_KEY, GOOGLE_TIMEZONE_API_KEY} from '../constants/constants';
import {getParamString, getOpenWeatherMapUrl, getTimezoneUrl} from './endpoints';

describe('Endpoints', () => {
  it('should translate undefined and {} into an empty param string', () => {
    expect(getParamString()).to.equal('');
    expect(getParamString({})).to.equal('');
  });

  it('should translate object into a param string', () => {
    const params = {
      key1: 'value1',
      key2: 'value2'
    };
    const result = '&key1=value1&key2=value2';
    expect(getParamString(params)).to.equal(result);
  });

  it('should assemble the weather endpoint URL with params', () => {
    const params = {
      key1: 'value1',
      key2: 'value2'
    };
    const url = getOpenWeatherMapUrl('endpoint')(params)('city');
    const prefix = 'http://api.openweathermap.org/data/2.5/';
    const paramString = getParamString(params);
    const result = `${prefix}endpoint?q=city&appid=${WEATHER_API_KEY}${paramString}`;
    expect(url).to.equal(result);
  });

  it('should assemble the google timezone API query', () => {
    const lon = 8.54;
    const lat	= 47.37;
    const timestamp = 1515601200;
    const url = getTimezoneUrl(lat, lon)(timestamp);

    const prefix = 'https://maps.googleapis.com/maps/api/timezone/json?location=';
    const result = `${prefix}${lat},${lon}&timestamp=${timestamp}&key=${GOOGLE_TIMEZONE_API_KEY}`;

    expect(url).to.equal(result);
  });
});
